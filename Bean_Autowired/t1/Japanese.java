package 罗斌310.t1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class Japanese implements People {
    @Resource(name = "saw")
    private Tool saw;
    @Qualifier("cat")
    @Autowired
    private Animal animal;

    @Override
    public void useTool() {
        System.out.println("日本人" + animal.testAnimal() + saw.chop());
    }
}
