package 罗斌310.t1;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Cat implements Animal {
    @Value("猫")
    private String msg;

    @Override
    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public String testAnimal() {
        return "带着" + msg;
    }
}
