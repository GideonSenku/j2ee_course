package 罗斌310.t1;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test_t1 {
    public static void main(String[] args) {
        String xmlPath = "罗斌310/t1/t1.xml";
        ApplicationContext context = new ClassPathXmlApplicationContext(xmlPath);
        People china = context.getBean("chinese",Chinese.class);
        People japan = context.getBean("japanese",Japanese.class);
        china.useTool();
        japan.useTool();
    }
}
