package 罗斌310.t1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class Chinese implements People {
    @Resource(name = "motor")
    private Tool motor;
    @Resource(name = "axe")
    private Tool axe;
    @Value("罗斌310")
    private String name;
    @Qualifier("dog")
    @Autowired
    private Animal animal;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void useTool() {
        System.out.println("中国人" + name + animal.testAnimal() + motor.chop() + "和" + axe.chop());
    }
}
