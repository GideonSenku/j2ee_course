package 罗斌310.t1;

import org.springframework.stereotype.Component;

@Component
public class Saw implements Tool {

    @Override
    public String chop() {
        return "用锯子砍柴";
    }
}
