package 罗斌310.t2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class AccountServiceImp implements AccountService {
	@Resource(name = "accountDaoImp")
	private AccountDao accountDao;

	@Override
	public List<Account> getAll(){
		return accountDao.getAll();
	}

	@Override
	public void save(Account account) {
		accountDao.save(account);
	}

	@Override
	public void delete(Integer id) {
		accountDao.delete(id);
		
	}

	@Override
	public void update(Account account) {
		accountDao.update(account);
		
	}

	@Override
	public Account getById(Integer id) {
		return accountDao.getById(id);
	}
	
	

}
