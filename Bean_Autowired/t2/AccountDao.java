package 罗斌310.t2;

import java.util.List;

public interface AccountDao {
    List<Account> getAll();

    void save(Account account);

    void delete(Integer id);

    void update(Account account);

    Account getById(Integer id);

}
