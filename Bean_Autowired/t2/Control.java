package 罗斌310.t2;

import org.springframework.stereotype.Controller;

import javax.annotation.Resource;
import java.util.List;

@Controller
public class Control {
    @Resource(name = "accountServiceImp")
    private AccountService accService;

    void getAll() {
        List<Account> acc = accService.getAll();
        for (Account a : acc) {
            System.out.println(a.toString());
        }
    }

    void save() {
        Account account = new Account();
        account.setName("310罗斌");
        account.setMoney((float) 1000);
        accService.save(account);
        System.out.printf("添加成功!");

    }

    void del() {
        accService.delete(1);
        System.out.printf("删除成功!");
    }

    Account getById(int id) {
        Account acc = accService.getById(id);
        if (acc == null) {
            System.out.println("not found");
        } else
            System.out.println(acc.toString());
        return acc;

    }

    void update(Account account) {
        accService.update(account);
        System.out.printf("修改成功!");
    }
}
