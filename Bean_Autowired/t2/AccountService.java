package 罗斌310.t2;

import java.util.List;

public interface AccountService {

	List<Account> getAll();

	Account getById(Integer id);

	void save(Account account);

	void update(Account account);

	void delete(Integer acccountId);
}