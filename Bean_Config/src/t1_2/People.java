package 罗斌310.t1_2;
import org.springframework.context.annotation.Configuration;
import 罗斌310.t1_1.Tool;

@Configuration
public class People {
    private Tool axe;

    public People(Tool axe) {
        this.axe = axe;
    }

    public void test() {
        System.out.println("罗斌310,构造方法注入实现");
        System.out.print("我在用" + axe.run());
    }
}
