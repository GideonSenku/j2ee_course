package 罗斌310.t2;

import org.springframework.context.annotation.Configuration;

import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

@Configuration
public class Customer {
    private List<String> schools;
    private Set axes;
    private Map scores;
    private Properties heath;

    public List<String> getSchools() {
        return schools;
    }

    public void setSchools(List<String> schools) {
        this.schools = schools;
    }

    public Set getAxes() {
        return axes;
    }

    public void setAxes(Set axes) {
        this.axes = axes;
    }

    public Map getScores() {
        return scores;
    }

    public void setScores(Map scores) {
        this.scores = scores;
    }

    public Properties getHeath() {
        return heath;
    }

    public void setHeath(Properties heath) {
        this.heath = heath;
    }
}
