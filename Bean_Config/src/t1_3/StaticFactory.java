package 罗斌310.t1_3;

import org.springframework.context.annotation.Bean;
import 罗斌310.t1_1.Axe;
import 罗斌310.t1_1.Motor;
import 罗斌310.t1_1.Saw;
import 罗斌310.t1_1.Tool;

public class StaticFactory {
    @Bean
    public static Tool getTool(String name){
        if (name.equalsIgnoreCase("axe")){
            return new Axe();
        }
        else if (name.equalsIgnoreCase("saw")){
            return new Saw();
        }
        else{
            return new Motor();
        }
    }
}
