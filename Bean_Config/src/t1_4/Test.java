package 罗斌310.t1_4;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import 罗斌310.t1_1.Tool;

public class Test {

    public static void main(String[] args) {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("罗斌310/t1_4/t1_4.xml");
        System.out.println("罗斌310,实例工厂方法注入实现");
        Tool a = ctx.getBean("Axe", Tool.class);
        System.out.println("我在用"+a.run());;

        Tool s = ctx.getBean("Saw", Tool.class);
        System.out.println("我在用"+s.run());

        Tool m = ctx.getBean("Motor", Tool.class);
        System.out.print("我在用"+m.run());
    }
}
