package 罗斌310.t1_1;

public class Axe implements Tool{
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String run() {
        return "用一把"+getName()+"在砍树";
    }
}
