package 罗斌310.t1_1;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {
    private static People p;
    public static void main(String[] args) {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("罗斌310/t1_1/t1_1.xml");
        p = ctx.getBean("t1_1",People.class);
        p.test();
    }
}