package 罗斌310.t1_1;

import org.springframework.context.annotation.Configuration;

@Configuration
public class People {
    private Tool tool;

    public void setTool(Tool tool) {
        this.tool = tool;
    }

    public void test(){
        System.out.println("罗斌310,设值注入实现");
        System.out.printf("我在用"+tool.run());
    }
}
