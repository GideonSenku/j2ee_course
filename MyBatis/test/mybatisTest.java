package 罗斌310.mybatis.test;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import 罗斌310.mybatis.entity.Customer;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class mybatisTest {
    public static void main(String[] args) throws IOException {
//        find(1);
//        findbyname("r");
//        insert();
//        update();
        delete(7);
    }
    public static void find(Integer id) throws IOException {
        SqlSession sqlSession = (SqlSession) sqlSes ();
        Customer c = sqlSession.selectOne("findCustomerById",id);
        System.out.println(c.toString());
        sqlSession.close();
    }

    public static void findbyname(String username) throws IOException {
        SqlSession sqlSession = (SqlSession) sqlSes ();
        List<Customer> cs = sqlSession.selectList("findCustomerByName",username);
        for (Customer c:cs) {
            System.out.println(c.toString());
        }
        sqlSession.close();
    }

    public static void insert() throws IOException {
        SqlSession sqlSession = (SqlSession) sqlSes ();
        Customer customer = new Customer();
        customer.setUsername("luobin");
        customer.setJobs("engineer");
        customer.setPhone("110");
        int result = sqlSession.insert("insertCustomer",customer);
        sqlSession.commit();
        if (result>0) {
            System.out.println("mybatisTest.insert successful");
        }
        else {
            System.out.println("mybatisTest.insert failed");
        }
        sqlSession.close();
    }

    public static void update() throws IOException {
        SqlSession sqlSession = (SqlSession) sqlSes ();
        Customer customer = new Customer();
        customer.setId(7);
        customer.setUsername("luobin");
        customer.setJobs("engineer");
        customer.setPhone("1908216412");
        int result = sqlSession.update("updateCustomer",customer);
        sqlSession.commit();
        if (result>0) {
            System.out.println("mybatisTest.update successful");
        }
        else {
            System.out.println("mybatisTest.update failed");
        }
        sqlSession.close();
    }

    public static void delete(Integer id) throws IOException {
        SqlSession sqlSession = (SqlSession) sqlSes ();
        int result = sqlSession.delete("deleteCustomer",id);
        sqlSession.commit();
        if (result>0) {
            System.out.println("mybatisTest.delete successful");
        }
        else {
            System.out.println("mybatisTest.delete failed");
        }
    }

    public static Object sqlSes() throws IOException {
        String resource = "罗斌310/mybatis/resource/config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        SqlSession sqlSession = sqlSessionFactory.openSession();
        return sqlSession;
    }

}
