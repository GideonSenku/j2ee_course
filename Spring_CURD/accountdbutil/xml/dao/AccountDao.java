package accountdbutil.xml.dao;

import java.util.List;

import accountdbutil.xml.domain.Account;

public interface AccountDao {
    List<Account> getAll();

    void save(Account account);

    void delete(Integer id);

    void update(Account account);

    Account getById(Integer id);

}
