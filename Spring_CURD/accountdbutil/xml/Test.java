package accountdbutil.xml;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import accountdbutil.xml.service.AccountService;
import accountdbutil.xml.domain.Account;

public class Test {
    static AccountService accService;

    public static void main(String[] args) {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("accountdbutil/xml/xmlbeans.xml");
        accService = ctx.getBean("accountService", AccountService.class);
//		save();
//		del();
//		Account account = getById(2);
//		account.setName("bbb");
//		account.setMoney((float) 3000);
//		update(account);
//        getAll();
        getById(3);
//        System.out.println("update money success");
    }

    static void getAll() {

        List<Account> acc = accService.getAll();
        for (Account a : acc) {
            System.out.println(a.toString());
        }

    }

    static void save() {
        Account account = new Account();
        account.setName("310罗斌");
        account.setMoney((float) 1000);
        accService.save(account);

    }

    static void del() {
        accService.delete(1);
    }

    static Account getById(int id) {
        Account acc = accService.getById(id);
        if (acc == null) {
            System.out.println("not found");
        } else
            System.out.println(acc.toString());
        return acc;

    }

    static void update(Account account) {
        accService.update(account);
    }
}
