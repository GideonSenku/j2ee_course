package accountdbutil.xml.service;

import java.util.List;
import accountdbutil.xml.dao.AccountDao;
import accountdbutil.xml.domain.Account;
public class AccountServiceImp implements AccountService {
	private AccountDao accountDao;
	
	public void setAccountDao(AccountDao accountDao) {
		this.accountDao = accountDao;
	}
	
	@Override
	public List<Account> getAll(){
		return accountDao.getAll();
	}

	@Override
	public void save(Account account) {
		accountDao.save(account);
	}

	@Override
	public void delete(Integer id) {
		accountDao.delete(id);
		
	}

	@Override
	public void update(Account account) {
		accountDao.update(account);
		
	}

	@Override
	public Account getById(Integer id) {
		return accountDao.getById(id);
	}
	
	

}
