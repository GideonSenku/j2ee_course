package accountdbutil.xml.service;
import java.util.List;

import accountdbutil.xml.domain.Account;

public interface AccountService {

	List<Account> getAll();

	Account getById(Integer id);

	void save(Account account);

	void update(Account account);

	void delete(Integer acccountId);
}