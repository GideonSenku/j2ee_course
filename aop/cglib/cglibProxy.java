package 罗斌310.cglib;

import org.springframework.cglib.proxy.Enhancer;
import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.cglib.proxy.MethodProxy;
import 罗斌310.jdk.Aspect;

import java.lang.reflect.Method;

public class cglibProxy implements MethodInterceptor {
    public Object createProxy(Object target){
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(target.getClass());
        enhancer.setCallback(this);
        return enhancer.create();
    }
    @Override
    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
        Aspect aspect = new Aspect();
        aspect.check_Permisson();
        Object obj = methodProxy.invokeSuper(o,objects);
        aspect.log();
        return obj;
    }
}
