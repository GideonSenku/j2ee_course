package 罗斌310.jdk;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class jdkProxy implements InvocationHandler {
    private UserDao userDao;

    public jdkProxy(UserDao userDao) {
        super();
        this.userDao = userDao;
    }

    public Object createProxy(Object target){
        return Proxy.newProxyInstance(userDao.getClass().getClassLoader(),userDao.getClass().getInterfaces(),this);
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Aspect aspect = new Aspect();
        aspect.check_Permisson();
        Object object = method.invoke(userDao,args);
        aspect.log();
        return object;
    }
}
