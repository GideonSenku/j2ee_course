package 罗斌310.Xml;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {
    static void springf1(){
        ApplicationContext context = new ClassPathXmlApplicationContext("罗斌310/Xml/aop.xml");
        HelloWorld hw1 = (HelloWorld) context.getBean("hw1");
        HelloWorld hw2 = (HelloWorld) context.getBean("hw2");
        hw1.printHelloWorld();
        System.out.println();
        hw1.doPrint();
        System.out.println();
        hw2.printHelloWorld();
        System.out.println();
        hw2.doPrint();
    }
    static void springf2(){
        ApplicationContext context = new ClassPathXmlApplicationContext("罗斌310/Xml/aop2.xml");
        HelloWorld hw1 = (HelloWorld) context.getBean("hw1");
        HelloWorld hw2 = (HelloWorld) context.getBean("hw2");
        hw1.printHelloWorld();
        System.out.println();
        hw1.doPrint();
        System.out.println();
        hw2.printHelloWorld();
        System.out.println();
        hw2.doPrint();
    }
    static void f(){

    }

    public static void main(String[] args) {
//        springf1();
        springf2();
    }
}
